import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Armin Hadzic</h1>
        <h2>18667</h2>
        <a
          className="App-link"
          href="https://www.youtube.com/watch?v=dQw4w9WgXcQ&ab_channel=RickAstley"
          target="_blank"
          rel="noopener noreferrer"
        >
          Nemoj ovdje kliknuti!
        </a>
      </header>
    </div>
  );
}

export default App;
